﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TodoAppMVC.Models;

using TodoAppMVC.Models.ViewModels;

namespace TodoAppMVC.Controllers
{
    public class HomeController : Controller
    {
        //data mock -> simulación de una base de datos
        private static List<Student> students = new List<Student>()
            {
                new Student() { FullName = "Pedro Aguirre", Address = "Chacabuco 1645", Birthday = DateTime.Today.AddYears(-20)},
                new Student() { FullName = "Juan Perez", Address = "Carrera 1645", Birthday = DateTime.Today.AddYears(-35)},
                new Student() { FullName = "Martin Ram", Address = "Rengo 158", Birthday = DateTime.Today.AddYears(-45)},
            };

        private static List<Teacher> teachers = new List<Teacher>()
        {
            new Teacher() { Dni = "18.677.972-k", Name = "Gustavo Rebolledo", Course = "PWEB"},
            new Teacher() { Dni = "18.677.964-9", Name = "Paulina Perez", Course = "POO"}
        };

        public HomeController()
        {
        }

        public IActionResult Index()
        {
            ViewData["subtitulo"] = "Lista de estudiantes y profesores";

            StudentViewModel viewModel = new StudentViewModel();
            viewModel.students = students;
            viewModel.teachers = teachers;

            return View(viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Contact()
        {
            return View();
        }
    }
}
