﻿using Microsoft.AspNetCore.Mvc;

namespace TodoAppMVC.Controllers
{
    public class OtherController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
