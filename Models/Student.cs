﻿using System;

namespace TodoAppMVC.Models
{
    public class Student
    {
        public string FullName { get; set; }
        public string Address { get; set; }
        public DateTime Birthday { get; set; }
    }
}
