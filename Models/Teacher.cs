﻿namespace TodoAppMVC.Models
{
    public class Teacher
    {
        public string Dni { get; set; }
        public string Name { get; set; }
        public string Course { get; set; }
    }
}
