﻿using System.Collections.Generic;

namespace TodoAppMVC.Models.ViewModels
{
    public class StudentViewModel
    {
        public List<Student> students { get; set; }

        public List<Teacher> teachers { get; set; }
    }
}
