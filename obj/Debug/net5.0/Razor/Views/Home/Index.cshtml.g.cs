#pragma checksum "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "67c345bd0597e1ec3c5bca8ced9f6637147fd69e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\_ViewImports.cshtml"
using TodoAppMVC;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\_ViewImports.cshtml"
using TodoAppMVC.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"67c345bd0597e1ec3c5bca8ced9f6637147fd69e", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1a617bb7fa4f83b3078d595254ef2af0747b04cd", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TodoAppMVC.Models.ViewModels.StudentViewModel>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n<div class=\"container-fluid\">\r\n    <h5");
            BeginWriteAttribute("class", " class=\"", 142, "\"", 150, 0);
            EndWriteAttribute();
            WriteLiteral("> ");
#nullable restore
#line 8 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
             Write(ViewData["subtitulo"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@" </h5>
    <div class=""row"">
        <div class=""col"">
            <table class=""table table-responsive"">
                <tr>
                    <th>
                        Nombre Completo
                    </th>
                    <th>
                        Dirección
                    </th>
                    <th>
                        Fecha de Nacimiento
                    </th>
                </tr>
");
#nullable restore
#line 23 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                 foreach(Student student in @Model.students)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr>\r\n                        <td>");
#nullable restore
#line 26 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                       Write(student.FullName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 27 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                       Write(student.Address);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 28 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                       Write(student.Birthday.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    </tr>    \r\n");
#nullable restore
#line 30 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </table>\r\n            <p");
            BeginWriteAttribute("class", " class=\"", 978, "\"", 986, 0);
            EndWriteAttribute();
            WriteLiteral(">Cantidad de estudiantes: ");
#nullable restore
#line 32 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                                            Write(Model.students.Count);

#line default
#line hidden
#nullable disable
            WriteLiteral(@" </p>
        </div>
        <div class=""col"">
             <table class=""table table-responsive"">
                <tr>
                    <th>
                        Rut
                    </th>
                    <th>
                        Nombre Completo
                    </th>
                    <th>
                        Asignatura
                    </th>
                </tr>
");
#nullable restore
#line 47 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                 foreach(Teacher teacher in @Model.teachers)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <tr>\r\n                        <td>");
#nullable restore
#line 50 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                       Write(teacher.Dni);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 51 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                       Write(teacher.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                        <td>");
#nullable restore
#line 52 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                       Write(teacher.Course);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                    </tr>    \r\n");
#nullable restore
#line 54 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </table>\r\n            <p");
            BeginWriteAttribute("class", " class=\"", 1785, "\"", 1793, 0);
            EndWriteAttribute();
            WriteLiteral(">Cantidad de profesores: ");
#nullable restore
#line 56 "C:\Users\grebolledoa\source\repos\LPW 2022\Seccion 2\ExampleMVC\Views\Home\Index.cshtml"
                                           Write(Model.teachers.Count);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </p>\r\n        </div>\r\n    </div>\r\n    \r\n   \r\n    \r\n</div>\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TodoAppMVC.Models.ViewModels.StudentViewModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
